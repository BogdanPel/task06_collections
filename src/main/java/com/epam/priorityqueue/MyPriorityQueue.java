package com.epam.priorityqueue;

import java.util.Arrays;

public class MyPriorityQueue<E extends Comparable<? super E>> {
  private E[] pq;
  private int size;
  private int capacity;
  private static final int DEFAULT_CAPACITY = 4;

  public MyPriorityQueue() {
    pq = (E[]) new Comparable[DEFAULT_CAPACITY];
    size = 0;
    capacity = DEFAULT_CAPACITY;
  }

  public MyPriorityQueue(int c) {
    pq = (E[]) new Comparable[c];
    size = 0;
    capacity = c;
  }

  public MyPriorityQueue(E[] inputs) {
    pq = Arrays.copyOf(inputs, inputs.length << 1);
    size = inputs.length;
    capacity = inputs.length << 1;
    for (int i = (inputs.length - 1) / 2; i >= 0; --i) {
      siftDown(i);
    }
  }

  private void swap(int idx1, int idx2) {
    E tmp = pq[idx1];
    pq[idx1] = pq[idx2];
    pq[idx2] = tmp;
  }

  private void siftUp(int idx) {
    int parent;
    while (idx != 0) {
      parent = (idx - 1) / 2;
      if (pq[parent].compareTo(pq[idx]) >= 0) break;
      swap(parent, idx);
      idx = parent;
    }
  }

  private void siftDown(int idx) {
    int kid;
    while (idx * 2 + 1 < size) {
      kid = idx * 2 + 1;
      if (idx * 2 + 2 < size && pq[idx * 2 + 2].compareTo(pq[kid]) > 0) kid = idx * 2 + 2;
      if (pq[idx].compareTo(pq[kid]) >= 0) break;
      swap(idx, kid);
      idx = kid;
    }
  }
  public boolean isEmpty() {
    return size == 0;
  }
  public int size() {
    return size;
  }

  public void add(E v) {
    pq[size++] = v;
    siftUp(size - 1);
  }

  public E poll() {
    E rst = pq[0];
    swap(0, --size);
    pq[size] = null;
    siftDown(0);
    return rst;
  }

  public E peek() {
    return pq[0];
  }

  @Override
  public String toString() {
    return Arrays.toString(pq);
  }
}
