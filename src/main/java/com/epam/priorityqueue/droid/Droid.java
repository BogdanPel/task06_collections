package com.epam.priorityqueue.droid;

import java.util.Objects;

public class Droid implements Comparable<Droid> {
  private int healthPoint;
  private int damage;

  public Droid(int healthPoint, int damage) {
    this.healthPoint = healthPoint;
    this.damage = damage;
  }

  public int getHealthPoint() {
    return healthPoint;
  }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setHealthPoint(int healthPoint) {
    this.healthPoint = healthPoint;
  }

  @Override
  public String toString() {
    return "Droid{" + "healthPoint=" + healthPoint + ", damage=" + damage + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Droid)) return false;
    Droid droid = (Droid) o;
    return healthPoint == droid.healthPoint &&
            damage == droid.damage;
  }

  @Override
  public int hashCode() {
    return Objects.hash(healthPoint, damage);
  }

  @Override
  public int compareTo(Droid other) {
    if (this.equals(other)) {
      return 0;
    } else if(getDamage() > other.getDamage())
        return 1;
    else return -1;
  }
}
