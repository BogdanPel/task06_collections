package com.epam.priorityqueue.droid;

import com.epam.priorityqueue.droid.Droid;

import java.util.ArrayList;
import java.util.List;

public class DroidShip<E extends Droid> {
    List<E> shipWithDroids;
    public DroidShip() {
     shipWithDroids = new ArrayList<>();
    }

    public void setShipWithDroids(List<E> shipWithDroids) {
        this.shipWithDroids = shipWithDroids;
    }

    public List<E> getAllDroids() {
        return shipWithDroids;
    }
    public void putDroid(E droid) {
            shipWithDroids.add(droid);
    }

}
