package com.epam.priorityqueue;


import com.epam.priorityqueue.droid.Droid;

public class PriorityQueueTest {
  public static void main(String[] args) {
    //

    Droid dron = new Droid(280, 48);
    Droid accelerator = new Droid(200, 50);
    Droid overPowered = new Droid(500, 100);
    Droid drodo = new Droid(180, 30);
    MyPriorityQueue<Droid> myPriorityQueue = new MyPriorityQueue<Droid>();
    myPriorityQueue.add(drodo);
    myPriorityQueue.add(accelerator);
    myPriorityQueue.add(overPowered);
    myPriorityQueue.add(dron);
    int count = 1;
    System.out.println(myPriorityQueue);
    while (!myPriorityQueue.isEmpty()) {
      System.out.println(myPriorityQueue);
      System.out.println("Dequeued" + count + " -->" + myPriorityQueue.poll());
      count++;
    }

    }
}
