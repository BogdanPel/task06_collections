package com.epam.container;

import java.util.Objects;

public class Player implements Comparable<Player> {
  String name;
  String role;

  public Player(String name, String role) {
    this.name = name;
    this.role = role;
  }

  public String getName() {
    return name;
  }

  public String getRole() {
    return role;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Player)) return false;
    Player player = (Player) o;
    return name.equals(player.name) && role.equals(player.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, role);
  }

  @Override
  public int compareTo(Player p) {
    if (getName().equals(p.getName())) {
      return 0;
    } else if (getName().compareTo(p.getName()) > 0) {
      return 1;
    } else return -1;
  }
}
