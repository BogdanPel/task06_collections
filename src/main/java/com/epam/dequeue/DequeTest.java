package com.epam.dequeue;

public class DequeTest {
  public static void main(String[] args) {
    Deque<String> myDeque = new DequeImpl<String>();
    String[] test1 = {"001", "002", "003", "004"};
    String[] test2 = {"005", "006", "007", "008"};
    String[] test3 = {"009", "010", "011", "012"};
    String[] test4 = {"013", "014", "015", "016"};
    System.out.println("Adding test1 to head.");
    for (String item : test1) {
      myDeque.insertFront(item);
    }
    System.out.println("Popping 1 element from head.");
    System.out.println(myDeque.removeFront());
    System.out.println("Popping 1 element from tail.");
    System.out.println(myDeque.removeLast());
    System.out.println("Adding test2 to head.");
    for (int i = 0; i < test1.length; i++) {
      myDeque.insertFront(test2[i]);
    }
    System.out.println("Popping all elements from head");
    for (int i = 0; myDeque.size() > 0; i++) {
      System.out.println(myDeque.removeFront());
    }
    System.out.println("Adding test3 to head.");
    for (String value : test3) {
      myDeque.insertFront(value);
    }
    System.out.println("Popping 1 element from tail.");
    System.out.println(myDeque.removeLast());
    System.out.println("Popping 1 element from head.");
    System.out.println(myDeque.removeLast());
    System.out.println("Adding test4 to tail.");
    for (String s : test4) {
      myDeque.insertLast(s);
    }
    System.out.println("Popping all elements from tail");
    for (int i = 0; myDeque.size() > 0; i++) {
      System.out.println(myDeque.removeLast());
    }
  }
}
