package com.epam.binarytree;

import java.util.*;
import java.util.function.BiConsumer;

public class MyBinaryTree<K, V> implements Map<K, V> {

  private final Comparator<? super K> comparator;

  private transient Entry<K, V> root;

  private transient int size = 0;

  public MyBinaryTree() {
    comparator = null;
  }

  public MyBinaryTree(Comparator<? super K> comparator) {
    this.comparator = comparator;
  }

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean containsKey(Object key) {
    return getEntry(key) != null;
  }

  public V get(Object key) {
    Entry<K, V> p = getEntry(key);
    return (p == null ? null : p.value);
  }

  final Entry<K, V> getEntry(Object key) {
    if (comparator != null) return getEntryUsingComparator(key);
    if (key == null) throw new NullPointerException();
    Comparable<? super K> k = (Comparable<? super K>) key;
    Entry<K, V> p = root;
    while (p != null) {
      int cmp = k.compareTo(p.key);
      if (cmp < 0) p = p.left;
      else if (cmp > 0) p = p.right;
      else return p;
    }
    return null;
  }

  final Entry<K, V> getEntryUsingComparator(Object key) {
    K k = (K) key;
    Comparator<? super K> cpr = comparator;
    if (cpr != null) {
      Entry<K, V> p = root;
      while (p != null) {
        int cmp = cpr.compare(k, p.key);
        if (cmp < 0) p = p.left;
        else if (cmp > 0) p = p.right;
        else return p;
      }
    }
    return null;
  }

  public boolean containsValue(Object value) {
    return false;
  }

  public V put(K key, V value) {
    Entry<K, V> t = root;
    if (t == null) {
      root = new Entry<K, V>(key, value, null);
      size = 1;
      return null;
    }
    int cmp;
    Entry<K, V> parent;
    Comparator<? super K> cpr = comparator;
    if (cpr != null) {
      do {
        parent = t;
        cmp = cpr.compare(key, t.key);
        if (cmp < 0) t = t.left;
        else if (cmp > 0) t = t.right;
        else return t.setValue(value);
      } while (t != null);
    } else {
      if (key == null) throw new NullPointerException();
      Comparable<? super K> k = (Comparable<? super K>) key;
      do {
        parent = t;
        cmp = k.compareTo(t.key);
        if (cmp < 0) t = t.left;
        else if (cmp > 0) t = t.right;
        else return t.setValue(value);
      } while (t != null);
    }
    Entry<K, V> e = new Entry<K, V>(key, value, parent);
    if (cmp < 0) parent.left = e;
    else parent.right = e;
    size++;
    return null;
  }

  public V remove(Object key) {
    Entry<K, V> p = getEntry(key);
    return p != null ? delete(p) : null;
  }

  private V delete(Entry<K, V> p) {
    Entry<K, V> right = p.right;
    Entry<K, V> left = p.left;
    Entry<K, V> parent = p.parent;
    if (parent != null) {
      if (parent.left == p) {
        parent.left = null;
      } else if (parent.right == p) {
        parent.right = null;
      }
      if (right != null) {
        putBranch(right);
      }
      if (left != null) {
        putBranch(left);
      }
    } else {
      if (left != null && right != null) {
        root = right;
        putBranch(left);
        putBranch(right);

      } else {
        root = right == null ? left : right;
      }
    }
    return p.value;
  }

  private Entry<K, V> putBranch(Entry<K, V> p) {
    put(p.key, p.value);
    if (p.left != null && p.right != null) {
      p = p != null ? putBranch(p.left) : null;
      p = p != null ? putBranch(p.right) : null;

    } else if (p.right != null || p.left != null) {
      p = p.right == null ? putBranch(p.left) : putBranch(p.right);
    }
    return null;
  }

  public void putAll(Map<? extends K, ? extends V> m) {}

  public void clear() {}

  public Set<K> keySet() {
    return null;
  }

  public Collection<V> values() {
    return null;
  }

  public Set<Map.Entry<K, V>> entrySet() {
    return null;
  }

  static final class Entry<K, V> implements Map.Entry<K, V> {
    K key;
    V value;
    Entry<K, V> left;
    Entry<K, V> right;
    Entry<K, V> parent;

    Entry(K key, V value, Entry<K, V> parent) {
      this.key = key;
      this.value = value;
      this.parent = parent;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }

    public V setValue(V value) {
      V oldValue = this.value;
      this.value = value;
      return oldValue;
    }

    public boolean equals(Object o) {
      if (!(o instanceof Map.Entry)) return false;
      Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;

      return isEqual(key, e.getKey()) && isEqual(value, e.getValue());
    }

    public int hashCode() {
      int keyHash = (key == null ? 0 : key.hashCode());
      int valueHash = (value == null ? 0 : value.hashCode());
      return keyHash ^ valueHash;
    }

    public String toString() {
      return key + "=" + value;
    }
  }

  static  boolean isEqual(Object o1, Object o2) {
    return (Objects.equals(o1, o2));
  }

  @Override
  public void forEach(BiConsumer<? super K, ? super V> action) {
    Objects.requireNonNull(action);
    for (Entry<K, V> e = root; e != null; e = successor(e)) {
      action.accept(e.key, e.value);
    }
  }

  static <K, V> Entry<K, V> successor(Entry<K, V> t) {
    if (t == null) return null;
    else if (t.right != null) {
      Entry<K, V> p = t.right;
      while (p.left != null) p = p.left;
      return p;
    } else {
      Entry<K, V> p = t.parent;
      Entry<K, V> ch = t;
      while (p != null && ch == p.right) {
        ch = p;
        p = p.parent;
      }
      return p;
    }
  }
}